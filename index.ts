import {
    componentCollector
} from './src'
import path from 'path';
import { adjustData } from './adjust-data';
const scanPathResolver = (filepath: string) => path.resolve(process.cwd(), filepath)
//ts-ignore
// const [nodeEnv, binPath, businessName, ...pathArray] = process.argv;
const businessNameScanPathMap: { [key: string]: string[] } = {
    'traveller': [
        '../../klook-fe/klook-nuxt-web/pages/traveller', 
        '../../klook-fe/klook-nuxt-web/components/traveller'
    ],
    'traveller-exclude':[
        '../../klook-fe/klook-nuxt-web/pages/traveller/activity',
        '../../klook-fe/klook-nuxt-web/components/traveller/activity'
    ],
    'hotel': [
        '../../klook-fe/klook-nuxt-web/pages/hotel', 
        '../../klook-fe/klook-nuxt-web/components/hotel'
    ],
    'experience': [
        '../../klook-fe/klook-nuxt-web/pages/experience', 
        '../../klook-fe/klook-nuxt-web/components/experience',
        '../../klook-fe/klook-nuxt-web/pages/traveller/activity',
        '../../klook-fe/klook-nuxt-web/components/traveller/activity'
    ],
    'entertainment': [
        '../../klook-fe/klook-nuxt-web/pages/event', 
        '../../klook-fe/klook-nuxt-web/pages/event-live',
        '../../klook-fe/klook-nuxt-web/pages/event-vertical',
        '../../klook-fe/klook-nuxt-web/components/event'
    ],
    'ptp': [
        '../../klook-fe/klook-nuxt-web/pages/ptp', 
        '../../klook-fe/klook-nuxt-web/components/ptp'
    ],
    'car-rental': [
        '../../klook-fe/klook-nuxt-web/pages/car-rental', 
        '../../klook-fe/klook-nuxt-web/components/car-rental'
    ],
    'klook-nuxt-web': [
        '../../klook-fe/klook-nuxt-web'
    ],
    'klook-whitelabel-web': [
        '../../klook-fe/klook-whitelabel-web'
    ],
    'klook-voucher-web': [
        '../../klook-fe/klook-voucher-web'
    ],
    // 'klook-agent-web': ['../../klook-fe/klook-agent-web', '../../klook-fe/klook-agent-web'],
    // 'klook-affiliate-front': ['../../klook-fe/klook-affiliate-web', '../../klook-fe/klook-affiliate-web'],
}

Promise.all( Object.keys(businessNameScanPathMap).map((businessName) => {
    return componentCollector({
        businessName: businessName,
        scanPaths: businessNameScanPathMap[businessName].map(relativePath => scanPathResolver(relativePath))
    })
})).then(()=>{
    adjustData();
})
