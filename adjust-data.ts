import fs from 'fs'
import path from 'path'
import { getTime } from './src/utils'
import { UsageDataType, handleComponentUsageData, writeDisk } from './src/index'

export function adjustData() {
    let filePath = path.join('./dist/', getTime(), 'traveller-usage-data.json'),
        travelerData: UsageDataType = JSON.parse(fs.readFileSync(filePath, 'utf-8')),
        excludeData: UsageDataType = JSON.parse(fs.readFileSync(path.join('./dist/', getTime(), 'traveller-exclude-usage-data.json'), 'utf-8'))

    Object.keys(excludeData.usageMap.components).forEach((componentName: string) => {
        travelerData.usageMap.components[componentName] -= excludeData.usageMap.components[componentName]
    })

    travelerData.total = 0;

    writeDisk(filePath, handleComponentUsageData(travelerData))
}