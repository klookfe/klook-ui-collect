import fs from 'fs'
import { promisifiedFileWalker } from './fileWalker'
import { ComponentNames as KlookUiComponentNames, MethodComponentMap as KlookUiMethodComponentMap, Directives, TransitionComponents } from './klook-ui-component'
import { getTime } from './utils';
import path from 'path';

export type UsageDataType = {
    total: number // 使用的组件总数
    totalKind: number // 使用的组件种类
    totalMap: {
        components: number,
        // directives: number,
    }
    usageMap: {
        components: {
            [key: string]: number
        },
        // directives: {
        //     [key: string]: number
        // }
    },
    projectName: string
    time: string
}

type ComponentCollectorType = {
    businessName: string,
    scanPaths: string[]
}

function mergeMethodToComponnent(componentsMap: UsageDataType['usageMap']['components']): UsageDataType['usageMap']['components'] {
    Object.keys(KlookUiMethodComponentMap).forEach((method) => {
        if (componentsMap[method]) {
            componentsMap[KlookUiMethodComponentMap[method]] = componentsMap[KlookUiMethodComponentMap[method]] || 0
            componentsMap[KlookUiMethodComponentMap[method]] += componentsMap[method]
            delete componentsMap[method]
        }
    })
    return componentsMap
}

export function componentCollector(context: ComponentCollectorType = {
    businessName: 'default-businessname',
    scanPaths: []
}) {

    let { businessName, scanPaths } = context,
        filter = (name) => KlookUiComponentNames.includes(name) || Object.keys(KlookUiMethodComponentMap).includes(name) || Directives.includes(name) || TransitionComponents.includes(name)

    function collectKlkComponents(files) {
        let componentUsageData: UsageDataType = {
            total: 0,
            totalKind: 0,
            totalMap: {
                components: 0,
                // directives: 0
            },
            usageMap: {
                components: {},
                // directives: {}
            },
            projectName: businessName,
            time: getTime()
        },
            componentRegex = /<(klk-[^\s>]+)|(\$\w+)|(v-[^=]+)/ig,// match: components | methods | directives
            dir = path.join(process.cwd(),`./dist/${componentUsageData.time}`);

        files.forEach(filePath => {
            let content = fs.readFileSync(filePath, 'utf8'),
                matchArr: string[] | null = [],
                componentType: 'components' | 'directives' = 'components',
                usageMap = {};

            while ((matchArr = componentRegex.exec(content)) !== null) {
                matchArr = matchArr.filter(matched => matched)
                if (filter && !filter(matchArr[1])) {
                    continue
                };

                if (matchArr[1].startsWith('v-')) {
                    componentType = 'directives';
                } else {
                    componentType = 'components'
                }
                usageMap = componentUsageData.usageMap[componentType]
                usageMap[matchArr[1]] ? usageMap[matchArr[1]]++ : (usageMap[matchArr[1]] = 1)
            }

        })

        componentUsageData.usageMap['components'] = mergeMethodToComponnent(componentUsageData.usageMap['components'])
        componentUsageData = handleComponentUsageData(componentUsageData)

        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir)
        }
        writeDisk(`${dir}/${businessName}-usage-data.json`, componentUsageData)
    }

    return Promise.all(
        scanPaths.map((scanPath) => promisifiedFileWalker(
            ['.vue', '.ts'],
            {
                root: scanPath,
                ignore: ['node_modules', 'dist', '.nuxt']
            }
        ))
    ).then(files => {
        return collectKlkComponents(files.reduce((prev, cur) => prev.concat(cur), []))
    }).catch(e => {
        console.error('Error :', e)
    })
}

export function writeDisk(filePath: string, data: UsageDataType) {
    fs.writeFile(filePath, JSON.stringify(data, null, 2), function (err) {
        if (err) {
            return console.error(err);
        }
    })
}

export function handleComponentUsageData(componentUsageData: UsageDataType): UsageDataType {
    Object.keys(componentUsageData.usageMap).forEach(componentType => {
        componentUsageData['totalMap'][componentType] =
            Object.keys(componentUsageData.usageMap[componentType]).reduce(
                (total, componentName) => {
                    componentType === 'components' && componentUsageData.totalKind++;
                    return total + componentUsageData.usageMap[componentType][componentName]
                }
                , componentUsageData['totalMap'][componentType])
    })

    componentUsageData['total'] = Object.keys(componentUsageData.totalMap).reduce
        ((total, key) => total + componentUsageData.totalMap[key]
            , componentUsageData['total'])

    return componentUsageData
}