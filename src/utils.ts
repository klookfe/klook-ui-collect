export function styleHyphenFormat(propertyName) {
    return propertyName.replace(/[A-Z]/g, function (match, offset, string) {
        return (offset > 0 ? '-' : '') + match.toLowerCase();
    });
}

export function getTime() {
    let date = new Date();
    return [date.getFullYear(), date.getMonth() + 1, date.getDate()].join('-')
    // return '2021-4-1'
}