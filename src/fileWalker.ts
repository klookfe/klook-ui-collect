import path from 'path'
import fs from 'fs'

type CallbackType = (err: null | Error, result: string[]) => void

export default function fileWalker(
    extnames: string[],
    options: {
        root: string,
        ignore: string[]
    } = {
            root: path.resolve(process.cwd(), '/'),
            ignore: []
        },
    done: CallbackType = () => { }
) {

    function walkDir(dir: string, done) {

        let files: string[] = []

        fs.readdir(dir, (err, results) => {

            if (err) {
                return done(err)
            }

            let pending = results.length;

            if (!pending) done(null, files);

            results.forEach((filename) => {
                // exclude results in ignore and startsWith '.'
                if (options.ignore.includes(filename) || filename.startsWith('.')) {
                    if (!--pending) done(null, files);
                    return;
                }

                let filePath = path.join(dir, filename),
                    stat = fs.lstatSync(filePath)

                if (stat.isFile()) {
                    // console.log(path.extname(filename))
                    if (extnames.includes(path.extname(filename))) {
                        files.push(filePath)
                    }

                    if (!--pending) done(null, files);

                } else if (stat.isDirectory()) {
                    walkDir(filePath, (err, res) => {

                        if (err) {
                            done(err);
                        }

                        files = files.concat(res)
                        if (!--pending) done(null, files);
                    })
                } else {
                    console.error('Unknown file type')
                }
            })
        })
    }

    walkDir(options.root, done)
}

function promisifyFileWalker(fn: typeof fileWalker) {
    return (...args: Parameters<typeof fileWalker>) => {
        let _resolve,
            _reject,
            originalCallback = args.length >= 3 && args.pop() || null,
            _promise = new Promise<string[]>((resolve, reject) => {
                _resolve = resolve;
                _reject = reject
            }),
            newCb = (...resultArgs: Parameters<CallbackType>) => {
                typeof originalCallback === 'function' && originalCallback.apply(null, resultArgs)
                if (resultArgs[0] != null) {
                    _reject(resultArgs[0])
                } else {
                    _resolve(resultArgs[1])
                }
            }

        args.push(newCb)

        fn.apply(null, args)

        return _promise
    }
}

export let promisifiedFileWalker = promisifyFileWalker(fileWalker)