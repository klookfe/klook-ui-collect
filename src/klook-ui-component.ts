import { styleHyphenFormat } from './utils'
// 2020-4-15 1.0.0 published
export const MethodComponentMap = {
    '$showLoading': 'klk-loading',
    '$alerts': 'klk-modal',
    '$confirm': 'klk-modal',
    '$prompt': 'klk-modal',
    '$notification': 'klk-notification',
    '$toast': 'klk-toast',
}

/**
 * ignore for now
 */
export const Directives = [
    // 'clickOutside',
    // 'resize',
    // 'scroll',
    // 'elevation',
].map(name => 'v-' + styleHyphenFormat(name))

export const TransitionComponents = [
    'ExpandTransition',
    'FadeTransition',
    'SlideTopTransition',
    'SlideBottomTransition',
    'SlideLeftTransition',
    'SlideRightTransition',
    'ScaleTransition',
].map(name => 'klk-' + styleHyphenFormat(name))

// copied from https://bitbucket.org/klook/klook-storybook/src/master/packages/klook-ui/src/index.js
export const ComponentNames = [
    'Alert',
    'Announcement',
    'Badge',
    'BottomSheet',
    'Breadcrumb',
    'Button',
    'CardSwiper',
    'Carousel',
    'Checkbox',
    'Collapse',
    'Counter',
    'DatePicker',
    'Divider',
    'Drawer',
    'Dropdown',
    'Form',
    'Grid',
    'Helpers',
    'Icon',
    'InfiniteScroll',
    'Input',
    'Link',
    'Loading',
    'Markdown',
    'Message',
    'Modal',
    'Notification',
    'Pagination',
    'Picker',
    'Poptip',
    'Progress',
    'Radio',
    'SectionTitle',
    'Select',
    'Skeleton',
    'Slider',
    'Steps',
    'Switch',
    'Table',
    'Tabs',
    'Tag',
    'TagSelect',
    'Toast',
    'Tree'
].map(name => 'klk-' + styleHyphenFormat(name))