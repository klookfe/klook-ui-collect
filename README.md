# pattern-collect
collect klook-ui component usages in project

## Usage
```
git clone git@bitbucket.org:klookfe/klook-ui-collect.git
nvm use v12
cd klook-ui-collect
npm install
// change ./src/utils 下的 getTime方法调整输出的目录时间（默认是当前时间）
npm run collect
// or just run custom collect
npm run custom-collect businessName scanPath1 scanPath2
```

//一个例子：
```
npm run custom-collect car-rental ../../klook-fe/klook-nuxt-web/pages/car-rental  ../../klook-fe/klook-nuxt-web/components/car-rental
```

会在dist目录下生成当前时间命名的文件夹，里面包含被扫描文件的路径

## 注意

1. 为了方便各个前端团队调用统计对比，目前约定的是统计每月1号的commit，所以在运行统计前（npm run collect ...前）记得把相关分支切换到当月的1号
## todos

1. auto generate on google sheet
