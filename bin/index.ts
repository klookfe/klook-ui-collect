import {
    componentCollector
} from '../src'
import path from 'path';
//ts-ignore
const [nodeEnv, binPath, businessName, ...pathArray] = process.argv;
console.log(`nodeEnv: ${nodeEnv}\n binPath: ${binPath} \n businessName: ${businessName}`)
if (pathArray.length) {
    if (!businessName) {
        throw Error('Please provide a valid businessName!!!')
    }

    componentCollector({
        businessName: businessName,
        scanPaths: pathArray.map(filepath => path.resolve(process.cwd(), filepath))
    })
} else {
    console.info(`\nHow to use? \neg: ./bin/pattern-collect ./klook-nuxt-web/pages/car-rental  ./klook-nuxt-web/components/car-rental`)
}